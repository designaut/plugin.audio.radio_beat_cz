# https://docs.python.org/2.7/
import os
import sys
import urllib
import urlparse
# http://mirrors.kodi.tv/docs/python-docs/
import xbmc
import xbmcaddon
import xbmcgui
import xbmcplugin
# http://docs.python-requests.org/en/latest/
import requests
import json

def buildUrl(query):
    base_url = sys.argv[0]
    return base_url + '?' + urllib.urlencode(query)

def buildStreamList(streams):
    streamList = []
    # iterate over the contents of the dictionary streams to build the list
    for stream in streams:
        # create a list item using the stream filename for the label
        li = xbmcgui.ListItem(label=streams[stream]['title'], thumbnailImage=streams[stream]['albumCover'])
        # set the fanart to the albumc cover
        li.setProperty('fanart_image', streams[stream]['albumCover'])
        # set the list item to playable
        li.setProperty('IsPlayable', 'true')
        # build the plugin url for Kodi
        # Example: plugin://plugin.audio.example/?url=http%3A%2F%2Fwww.theaudiodb.com%2Ftestfiles%2F01-pablo_perez-your_ad_here.mp3&mode=stream&title=01-pablo_perez-your_ad_here.mp3
        url = buildUrl({'mode': 'stream', 'url': streams[stream]['url'], 'title': streams[stream]['title']})
        # add the current list item to a list
        streamList.append((url, li, False))
    # add list to Kodi per Martijn
    # http://forum.kodi.tv/showthread.php?tid=209948&pid=2094170#pid2094170
    xbmcplugin.addDirectoryItems(addonHandle, streamList, len(streamList))
    # set the content of the directory
    xbmcplugin.setContent(addonHandle, 'songs')
    xbmcplugin.endOfDirectory(addonHandle)

def getSongTitle():
    jsonString = requests.get(nowPlayingInfoUrl).text
    songTitle = json.loads(jsonString)['artist'] + ': ' + json.loads(jsonString)['song']
    return songTitle

def updateSongTitle():
    songTitle = getSongTitle()
    # TODO: update song title with new value
    return songTitle

def playStream(url):
    # set the path of the stream to a list item
    playItem = xbmcgui.ListItem(path=url)
    playItem.setLabel(getSongTitle())
    # the list item is ready to be played by Kodi
    xbmcplugin.setResolvedUrl(addonHandle, True, listitem=playItem)

def main():
    global player
    args = urlparse.parse_qs(sys.argv[2][1:])
    mode = args.get('mode', None)

    # initial launch of add-on
    if mode is None:
        # display the list of streams in Kodi
        buildStreamList(streams)
        player = MyPlayer()
    # a stream from the list has been selected
    elif mode[0] == 'stream':
        # pass the url of the stream to playStream
        playStream(args['url'][0])

class MyPlayer(xbmc.Player):
    """docstring for MyPlayer.xbmc.Playeref """
    keepAddonAlive = False

    def __init__(self, *args, **kwargs):
        self.keepAddonAlive = True
        xbmc.log(addonName + ' | Player inited', xbmc.LOGNOTICE)
        super(MyPlayer, self).__init__(*args, **kwargs)

    def onPlayBackStarted(self):
        xbmc.log(addonName + ' | Playback is started', xbmc.LOGNOTICE)

    def onPlayBackStopped(self):
        self.keepAddonAlive = False
        xbmc.log(addonName + ' | Playback is stopped', xbmc.LOGNOTICE)

    def onPlayBackPaused(self):
        xbmc.log(addonName + ' | Playback is paused', xbmc.LOGNOTICE)

    def onPlayBackResumed(self):
        xbmc.log(addonName + ' | Playback is resumed', xbmc.LOGNOTICE)

    def getAliveFlag(self):
        return self.keepAddonAlive

if __name__ == '__main__':
    radioStreamUrl = 'http://icecast2.play.cz/radiobeat128.mp3'
    radioLogoUrl = 'http://api.play.cz/static/radio_logo/t90/beat.png'
    nowPlayingInfoUrl = 'https://onair.play.cz/json/beat.json'
    doReadNowPlayingInfo = True

    addon       = xbmcaddon.Addon()
    addonName   = addon.getAddonInfo('name')
    player = None

    albumCover = radioLogoUrl.replace(' ', '%20')
    songTitle = 'Radio Beat 128MBs'
    streams = {1: {'albumCover': albumCover, 'title': songTitle, 'url': radioStreamUrl}}

    addonHandle = int(sys.argv[1])
    main()

    # only on addon init, will not loop when mode == 'stream'
    # TODO: do it better
    if player != None :
        while player.getAliveFlag():
            songTitle = updateSongTitle()
            xbmc.sleep(1000)

        xbmc.log(addonName + ' | Addon exit', xbmc.LOGNOTICE)
